import { useSelector } from "react-redux";

const About = () => {
  const globalState = useSelector((state) => state);
  console.log(globalState);
  return (
    <>
      <h1>Halaman About</h1>
      <p>{globalState.auth.name}</p>
      <p>{globalState.auth.username}</p>
    </>
  );
};

export default About;