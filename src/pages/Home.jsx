import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { useHistory } from 'react-router-dom';
// import axios from "axios";
import { getName, getUsername } from "./../redux/action";

const Home = () => {
  // const history = useHistory();
  const dispatch = useDispatch();
    // const currentToken = localStorage.getItem("token");
  const globalState = useSelector((state) => state);
  console.log(globalState);
  
  // const handleAuth = async () => {
  //   if(currentToken){
  //     try{
  //       const config = {
  //         headers: {
  //           Authorization: `Bearer ${currentToken}`
  //         },
  //       };
  //       const response = await axios.get(`${process.env.REACT_APP_BASE_API_URL}/auth/profile`, config);
  //       dispatch(getName(response.data.result.name));
  //       dispatch(getUsername(response.data.result.username));
  //     } catch (err) {
  //       history.push('/');
  //       console.log(err);
  //     }
  //   } else {
  //     history.push('/');
  //   };
  // };

  useEffect(() => {
    dispatch(getUsername());
  }, []);

  return (
    <>
      <h1>Halaman Home</h1>
      {globalState.auth.name} <br />
      {globalState.auth.username}
    </>
  );
};

export default Home;